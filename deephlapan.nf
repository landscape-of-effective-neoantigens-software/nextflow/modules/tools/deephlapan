process deephlapan {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'deephlapan_container'
  label 'deephlapan'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${norm_run}_${tumor_run}/deephlapan"
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(inp_file)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*predicted_result.csv"), emit: deephlapan_scores
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*predicted_result_rank.csv"), emit: deephlapan_scores_w_ranks

  script:
  """
  deephlapan -F ${inp_file} ${parstr}
#  NUM_LINES=`wc -l ${inp_file} | cut -f 1 -d ' '`
#  AVG_NUM_LINES=\$((\${NUM_LINES}/${task.cpus}))
#
#  tail -n +2 ${inp_file} | split -d -l \${AVG_NUM_LINES} - --filter='sh -c "{ head -n1 ${inp_file}; cat; } > \$FILE"'
# 
#  for i in `ls x*`; do  
#    deephlapan -F \${i} ${parstr}
#  done
#
#  wait
#
#  head -1 x00_predicted_result.csv > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.deephlapan.predicted_result.csv
#  for i in `ls x*_predicted_results.csv`; do
#     cat \${i} >> ${dataset}-${pat_name}-${norm_run}_${tumor_run}.deephlapan.predicted_result.csv
#  done 
#
#  head -1 x00_predicted_result_rank.csv > ${dataset}-${pat_name}-${norm_run}_${tumor_run}.deephlapan.predicted_result_rank.csv
#  for i in `ls x*_predicted_results_rank.csv`; do
#     cat \${i} >> ${dataset}-${pat_name}-${norm_run}_${tumor_run}.deephlapan.predicted_result_rank.csv
#  done 
  """
}

process deephlapan_make_input_file {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label 'deephlapan_container'
  label 'deephlapan'
  cache 'lenient'

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(peptides), path(alleles)
  val comma_sep_lens

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*deephlapan_inp.tsv"), emit: deephlapan_inpf

  script:
  """
#!/usr/bin/env python3

import itertools
import re

peptide_lens = [int(x) for x in "${comma_sep_lens}".split(',')]
print("Peptide lengths: {}".format(peptide_lens))


alleles = []
with open("${alleles}", 'r') as allele_inf:
    alleles = allele_inf.readline().rstrip().split(',')
print("Alleles: {}".format(alleles))

peptides = {}
with open("${peptides}", 'r') as pep_inf:
    for line in pep_inf:
       if re.search('^>', line):
           next(pep_inf) #Skipping comment line.
           peptides[line.rstrip().split(' ')[0].replace(':', '_').replace('>', '')] = next(pep_inf).rstrip()

def sliding_window(iterable, n):
    iterables = itertools.tee(iterable, n)

    for iterable, num_skipped in zip(iterables, itertools.count()):
        for _ in range(num_skipped):
            next(iterable, None)

    joind_peps = [''.join(x) for x in zip(*iterables)]

    return list(set(joind_peps))

pep_kmers = {}

for id, peptide in peptides.items():
    pep_kmers[id] = []
    if len(peptide) in peptide_lens:
        pep_kmers[id].append(''.join(peptide))
    else:
        for peptide_len in peptide_lens:
            pep_kmers[id].extend(sliding_window(peptide, peptide_len))

print("Peptide kmers: {}".format(pep_kmers))

with open("${dataset}-${pat_name}-${norm_run}_${tumor_run}.deephlapan_inp.tsv", 'w') as outf:
    outf.write("Annotation,HLA,peptide\\n")
    for id, sub_pep_kmers in pep_kmers.items():
        for sub_pep_kmer in sub_pep_kmers:
            if not(re.search('\\*', sub_pep_kmer)):
                for allele in alleles:
                    outf.write("{},{},{}\\n".format(id, allele, sub_pep_kmer))
  """
}
